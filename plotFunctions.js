const { DateTime, Duration } = luxon
const plot = {}

plot.plan = (name, color) => fetch(name + '.json')
  .then(response => response.json())
  .then(latlngs => {
    const totalDistance = L.polyline(latlngs)._latlngs
      .map((p, i, a) => a[i + 1] && p.distanceTo(a[i + 1]) || 0)
      .reduce((a, b) => a + b)
    const plan = L.featureGroup().addTo(map)
    let distance = 0
    latlngs.forEach((_p, i, a) => {
      const leg = L.polyline(a.slice(i, i + 2), { color })
        .addTo(plan)
      distance += leg._latlngs[1] && leg._latlngs[0].distanceTo(leg._latlngs[1]) || 0
      leg.bindTooltip(
        `${Math.round(distance)} m \u2014 ${Math.round(distance / totalDistance * 100)}%`,
        { sticky: true }
      )
    })
    L.marker(latlngs.slice(-1)[0])
      .addTo(map)
      .bindPopup(`Distance: ${Math.round(totalDistance)} m`)
      .openPopup()
    map.fitBounds(plan.getBounds())
  })

plot.cpsPlan = (cps, color) => fetch(url.searchParams.get('cps') + '.json')
  .then(response => response.json())
  .then(allLatlngs => {
    const latlngs = ['S/F', ...cps.split('-'), 'S/F']
      .map(plannedCp => allLatlngs.filter(cp => cp.comment === plannedCp)).flat()
    const totalDistance = L.polyline(latlngs)._latlngs
      .map((p, i, a) => a[i + 1] && p.distanceTo(a[i + 1]) || 0)
      .reduce((a, b) => a + b)
    const plan = L.featureGroup().addTo(map)
    let distance = 0
    latlngs.forEach((_p, i, a) => {
      const leg = L.polyline(a.slice(i, i + 2), { color })
        .addTo(plan)
      distance += leg._latlngs[1] && leg._latlngs[0].distanceTo(leg._latlngs[1]) || 0
      leg.bindTooltip(
        `${Math.round(distance)} m \u2014 ${Math.round(distance / totalDistance * 100)}%`,
        { sticky: true }
      )
    })
    L.marker(latlngs.slice(-1)[0])
      .addTo(map)
      .bindPopup(`Distance: ${Math.round(totalDistance)} m`)
      .openPopup()
    map.fitBounds(plan.getBounds())
  })

plot.cps = name => fetch(name + '.json')
  .then(response => response.json())
  .then(latlngs => {
    const markers = L.featureGroup().addTo(map)
    latlngs.forEach(cp => {
      L.circleMarker(cp, {
        color: 'magenta',
        fill: false,
        radius: 15,
      })
        .bindTooltip(cp.comment, {
          direction: 'right',
          className: 'cp-tooltip',
          offset: [5, 10],
          permanent: true,
        })
        .addTo(markers)
    })
    map.fitBounds(markers.getBounds())
  })

plot.gpxCps = name => fetch(name + '.gpx')
  .then(res => res.text()).then(data => {
    const gpx = new gpxParser()
    gpx.parse(data)
    const markers = L.featureGroup().addTo(map)
    gpx.waypoints.forEach(cp => {
      L.circleMarker([cp.lat, cp.lon], {
        color: 'magenta',
        fill: false,
        radius: 15,
      })
        .bindTooltip(cp.name, {
          direction: 'right',
          className: 'cp-tooltip',
          offset: [5, 10],
          permanent: true,
        })
        .addTo(markers)
    })
    map.fitBounds(markers.getBounds())
  })

// tested with .tcx files downloaded from Fitbit or Garmin Connect
plot.tcx = (name, color) => fetch(name + '.tcx')
  .then(res => res.text()).then(res => $.parseXML(res))
  .then(tcx => {
    const latlngs = []
    const pointsData = []
    let distance = 0
    $('Trackpoint:has(Position)', tcx).each((_, point) => {
      latlngs.push([
        $('LatitudeDegrees', point).text(),
        $('LongitudeDegrees', point).text()
      ])
      distance = Math.max(distance, Math.round($('DistanceMeters', point).text()))
      pointsData.push({
        timestamp: DateTime.fromISO($('Time', point).text()).toLocaleString(DateTime.DATETIME_MED_WITH_SECONDS),
        distance,
        altitude: Math.round($('AltitudeMeters', point).text()),
        hr: $('HeartRateBpm Value', point).text()
      })
    })
    const time = Duration.fromMillis(
      +$('TotalTimeSeconds', tcx).toArray()
        .map(({ innerHTML }) => innerHTML)
        .reduce((a, b) => Number(a) + Number(b)) * 1000
    ).toFormat('h:mm:ss')
    L.marker(latlngs.slice(-1)[0])
      .addTo(map)
      .bindPopup(`Time: ${time}<br />Distance: ${distance} m`)
      .openPopup()
    const route = L.featureGroup().addTo(map)
    const step = Math.round(latlngs.length / 2000) || 1
    for (let i = 0; i < latlngs.length; i = i + step) {
      let tooltip = pointsData[i].timestamp
      tooltip += `<br />Distance: ${pointsData[i].distance} m`
      tooltip += `<br />Altitude: ${pointsData[i].altitude} m`
      tooltip += pointsData[i].hr && `<br />HR: ${pointsData[i].hr} bpm`
      L.polyline(latlngs.slice(i, i + step + 1), { color })
        .bindTooltip(tooltip, { sticky: true })
        .addTo(route)
    }
    map.fitBounds(route.getBounds())
  })

// tested with .gpx files exported from Garmin Connect
plot.gpx = (name, color) => fetch(name + '.gpx')
  .then(res => res.text()).then(data => {
    const gpx = new gpxParser()
    gpx.parse(data)
    const hrData = $('trkpt ns3\\:hr', $.parseXML(data))
      .map((_, { innerHTML }) => innerHTML)
    const route = L.featureGroup().addTo(map)
    L.marker(gpx.tracks[0].points.slice(-1)[0]).addTo(map)
      .bindPopup(`Distance: ${gpx.tracks[0].distance.total.toFixed()} m<br />${name}`)
      .openPopup()
    const step = Math.round(gpx.tracks[0].points.length / 2000) || 1
    const startTime = DateTime.fromJSDate(gpx.tracks[0].points[0].time)
    for (let i = 0; i < gpx.tracks[0].points.length; i = i + step) {
      const time = DateTime.fromJSDate(gpx.tracks[0].points[i].time)
      let tooltip = time.toLocaleString(DateTime.DATETIME_MED_WITH_SECONDS)
      tooltip += '<br />Time: ' + time.diff(startTime).toFormat('h:mm:ss')
      tooltip += `<br />Distance: ${gpx.tracks[0].distance.cumul[i].toFixed()} m`
      tooltip += `<br />Altitude: ${gpx.tracks[0].points[i].ele?.toFixed()} m`
      if (hrData[i]) { tooltip += `<br />HR: ${hrData[i]} bpm` }
      L.polyline(gpx.tracks[0].points.slice(i, i + step + 1), { color })
        .bindTooltip(tooltip, { sticky: true })
        .addTo(route)
    }
    map.fitBounds(route.getBounds())
  })

// lap start positions are turned into CPs, tested with .tcx files exported from Garmin Connect
plot.tcxCps = name => fetch(name + '.tcx')
  .then(res => res.text()).then(res => $.parseXML(res))
  .then(tcx => {
    const markers = L.featureGroup().addTo(map)
    $('Lap', tcx).each((i, point) => {
      L.circleMarker([
        $('Position:first() LatitudeDegrees', point).text(),
        $('Position:first() LongitudeDegrees', point).text()
      ], {
        color: 'magenta',
        fill: false,
        radius: 15,
      })
        .bindTooltip(i.toString(), {
          direction: 'right',
          className: 'cp-tooltip',
          offset: [5, 10],
          permanent: true,
        })
        .addTo(markers)
    })
    map.fitBounds(markers.getBounds())
  })