import json
import math
import os
import sys
import urllib.request
from wand.image import Image
from wand.drawing import Drawing
from wand.color import Color


def deg2tile(lat, lng, zoom):
  n = 2 ** zoom
  x = (lng + 180) / 360 * n
  y = (1 - math.asinh(math.tan(math.radians(lat))) / math.pi) / 2 * n
  return [x, y]


try:
  loc = sys.argv[1]
except:
  print('Give location as the first parameter')
  sys.exit(1)
try:
  provider = sys.argv[2]
except:
  print('Give provider ("okaart", "maaamet" or "foto") as the second parameter')
  sys.exit(1)
try:
  with open(loc + '.json') as f:
    cps = json.load(f)
except:
  print(f'{loc}.json not found')
  sys.exit(1)
try:
  os.mkdir(loc)
except:
  print(f'Couldn\'t create folder \'{loc}\'')

if sys.argv[2] == 'okaart':
  zoom = 14
  url = 'https://okaart.osport.ee/'
  extension = 'png'
  tms = False
elif sys.argv[2] == 'maaamet':
  zoom = 14
  url = 'https://tiles.maaamet.ee/tm/tms/1.0.0/kaart@GMC/'
  extension = 'jpg'
  tms = True
elif sys.argv[2] == 'foto':
  zoom = 18
  url = 'https://tiles.maaamet.ee/tm/tms/1.0.0/foto@GMC/'
  extension = 'jpg'
  tms = True

upper_left_tile = [int(val) for val in deg2tile(
  max([cp.get('lat') for cp in cps]),
  min([cp.get('lng') for cp in cps]),
  zoom)]
bottom_right_tile = [int(val) for val in deg2tile(
  min([cp.get('lat') for cp in cps]),
  max([cp.get('lng') for cp in cps]),
  zoom)]
rows = range(upper_left_tile[0], bottom_right_tile[0] + 2)
columns = range(upper_left_tile[1], bottom_right_tile[1] + 1)

for tile_x in rows:
  for tile_y in columns:
    urllib.request.urlretrieve(
      f'{url}{zoom}/{tile_x}/{2 ** zoom - tile_y - 1 if tms else tile_y}.{extension}',
      f'{loc}/{tile_x}_{tile_y}.{extension}')

with Drawing() as draw:
  draw.font_size = 48
  draw.stroke_width = 3
  for cp in cps:
    cp_tile = deg2tile(cp['lat'], cp['lng'], zoom)
    x = int((cp_tile[0] - rows[0]) * 256)
    y = int((cp_tile[1] - columns[0]) * 256)
    draw.push()
    draw.fill_color = Color('none')
    draw.stroke_color = Color('magenta')
    draw.ellipse((x, y), (24, 24))
    draw.pop()
    draw.push()
    draw.fill_color = Color('magenta')
    if 'comment' in cp: draw.text(x + 32, y + 32, cp['comment'])
    draw.pop()
  with Image(width=len(rows) * 256, height=len(columns) * 256) as img:
    x = 0
    for tile_x in rows:
      y = 0
      for tile_y in columns:
        with Image(filename=f'{loc}/{tile_x}_{tile_y}.{extension}') as frag:
          img.composite(frag, x, y)
        y += 256
      x += 256
    draw(img)
    img.save(filename=f'{loc}.png')